module salsa.debian.org/mdosch/go-sendxmpp

go 1.21.5

require (
	github.com/ProtonMail/gopenpgp/v2 v2.7.5
	github.com/beevik/etree v1.3.0
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/pborman/getopt/v2 v2.1.0
	github.com/xmppo/go-xmpp v0.1.5-0.20240402113945-0ae62a33a21d
	salsa.debian.org/mdosch/xmppsrv v0.2.6
)

require (
	github.com/ProtonMail/go-crypto v1.0.0 // indirect
	github.com/ProtonMail/go-mime v0.0.0-20230322103455-7d82a3887f2f // indirect
	github.com/cloudflare/circl v1.3.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
