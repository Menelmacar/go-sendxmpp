# go-sendxmpp

## about

A tool to send messages to an XMPP contact or MUC inspired by [sendxmpp](https://sendxmpp.hostname.sk/).
You can find other sendxmpp alternatives in the [XSF wiki](https://wiki.xmpp.org/web/User:MDosch/Sendxmpp_incarnations).

## support

You might join the [chat](https://join.jabber.network/#go-sendxmpp@chat.mdosch.de?join) if you have problems, 
want to contribute or just want to talk about the project. You might also talk about any of the other 
sendxmpp incarnations. :)

[![Go-sendxmpp MUC badge](https://chat.mdosch.de/muc_badge/go-sendxmpp@chat.mdosch.de)](https://join.jabber.network/#go-sendxmpp@chat.mdosch.de?join)

## requirements

* [go](https://golang.org/) >= 1.21

## installation

### repositories including go-sendxmpp

[![Packaging status](https://repology.org/badge/vertical-allrepos/go:sendxmpp.svg)](https://repology.org/project/go:sendxmpp/versions)

### manual installation

Latest release:

```plain
$ go install salsa.debian.org/mdosch/go-sendxmpp@latest
```

Current development version:

```plain
$ go install salsa.debian.org/mdosch/go-sendxmpp@master
```

You will find the binary in `$GOPATH/bin` or, if set, `$GOBIN`.

### binaries

There are some (automatically built and untested) binaries linked to the
[release](https://salsa.debian.org/mdosch/go-sendxmpp/-/releases).

## usage

You can either pipe a programs output to `go-sendxmpp`, write in your terminal (put \^D in a new
line to finish) or send the input from a file (`-m` or `--message`).

The account data is expected at `~/.config/go-sendxmpp/config` if no other configuration file location 
is specified with `-f` or `--file`. The configuration file is expected to be in the following format:

```plain
username: <your_jid>
password: <your_jabber_password>
```

If this is not sufficient to connect you might also specify `jserver` and `port`. It is also possible to
use a password manager. In this case the `password` setting should be replaced by the `eval_password`
setting:    

```
eval_password: <command_to_unlock_your_password>
```

You can also configure the alias/nickname for MUCs via the `alias` setting:

```
alias: <my_alias>
```

If no configuration file is present or if the values should be overridden it is possible to define 
the account details via command line options:

```plain
Usage: go-sendxmpp [-cdilnt] [-a value] [-f value] [--headline] [--help] [-h value] [-j value] [-m value] [--muc-password value] [--oob-file value] [--ox] [--ox-delete-nodes] [--ox-genprivkey-rsa] [--ox-genprivkey-x25519] [--ox-import-privkey value] [--ox-passphrase value] [-p value] [--raw] [--scram-mech-pinning value] [--ssdp-off] [--timeout value] [--tls-version value] [-u value] [--version] [recipients…]
 -a, --alias=value  Set alias/nicknamefor chatrooms.
 -c, --chatroom     Send message to a chatroom.
 -d, --debug        Show debugging info.
 -f, --file=value   Set configuration file. (Default:
                    ~/.config/go-sendxmpp/sendxmpprc)
     --headline     Send message as type headline.
     --help         Show help.
 -h, --http-upload=value
                    Send a file via http-upload.
 -i, --interactive  Interactive mode (for use with e.g. 'tail -f').
 -j, --jserver=value
                    XMPP server address.
 -l, --listen       Listen for messages and print them to stdout.
 -m, --message=value
                    Set file including the message.
     --muc-password=value
                    Password for password protected MUCs.
 -n, --no-tls-verify
                    Skip verification of TLS certificates (not recommended).
     --oob-file=value
                    URL to send a file as out of band data.
     --ox           Use "OpenPGP for XMPP" encryption (experimental).
     --ox-delete-nodes
                    Delete existing OpenPGP nodes on the server.
     --ox-genprivkey-rsa
                    Generate a private OpenPGP key (RSA 4096 bit) for the given
                    JID and publish the corresponding public key.
     --ox-genprivkey-x25519
                    Generate a private OpenPGP key (x25519) for the given JID
                    and publish the corresponding public key.
     --ox-import-privkey=value
                    Import an existing private OpenPGP key.
     --ox-passphrase=value
                    Passphrase for locking and unlocking the private OpenPGP
                    key.
 -p, --password=value
                    Password for XMPP account.
     --raw          Send raw XML.
     --scram-mech-pinning=value
                    Enforce the use of a certain SCRAM authentication mechanism.
     --ssdp-off     Disable XEP-0474: SASL SCRAM Downgrade Protection.
     --timeout=value
                    Connection timeout in seconds. [10]
 -t, --tls          Use direct TLS.
     --tls-version=value
                    Minimal TLS version. 10 (TLSv1.0), 11 (TLSv1.1), 12
                    (TLSv1.2) or 13 (TLSv1.3). [12]
 -u, --username=value
                    Username for XMPP account.
     --version      Show version information.
```

### examples

Send a message to two recipients using a configuration file.

```bash
cat message.txt | ./go-sendxmpp -f ./sendxmpp recipient1@example.com recipient2@example.com
```

Send a message to two recipients directly defining account credentials.

```bash
cat message.txt | ./go-sendxmpp -u bob@example.com -p swordfish recipient1@example.com recipient2@example.com
```

Send a message to two groupchats (`-c`) using a configuration file.

```bash
cat message.txt | ./go-sendxmpp -cf ./sendxmpp chat1@conference.example.com chat2@conference.example.com
```

Send file changes to two groupchats (`-c`) using a configuration file.

```bash
tail -f example.log | ./go-sendxmpp -cif ./sendxmpp chat1@conference.example.com chat2@conference.example.com
```

### shell completion

#### zsh

There are no shell completions yet (contributions welcome) but for zsh it is possible to automatically create completions from `--help` which might work *good enough*.

Just place the following in your `~/.zshrc` or `~/.zshrc.local`:

``` bash
compdef _gnu_generic go-sendxmpp
```
