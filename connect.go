// Copyright Martin Dosch.
// Use of this source code is governed by the BSD-2-clause
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"net"
	"os"
	"strings"

	"github.com/xmppo/go-xmpp"        // BSD-3-Clause
	"salsa.debian.org/mdosch/xmppsrv" // BSD-2-Clause
)

func connect(options xmpp.Options, directTLS bool) (*xmpp.Client, error) {
	proxy := os.Getenv("HTTP_PROXY")
	server := options.User[strings.Index(options.User, "@")+1:]
	// Look up SRV records if server is not specified manually.
	if options.Host == "" {
		// Don't do SRV look ups if proxy is set.
		if proxy == "" {
			// Look up xmpp-client SRV records.
			srvMixed, err := xmppsrv.LookupClient(server)
			if len(srvMixed) > 0 && err == nil {
				for _, adr := range srvMixed {
					if !directTLS && adr.Type != "xmpps-client" {
						// Use StartTLS
						options.NoTLS = true
						options.StartTLS = true
					} else if adr.Type == "xmpps-client" {
						// Use direct TLS
						options.NoTLS = false
						options.StartTLS = false
					}
					options.Host = net.JoinHostPort(adr.Target, fmt.Sprint(adr.Port))
					// Connect to server
					client, err := options.NewClient()
					if err == nil {
						return client, nil
					}
				}
			}
		}
	}
	_, port, _ := net.SplitHostPort(options.Host)
	if port == "" {
		if options.Host == "" {
			options.Host = server
		}
		// Try port 5223 if directTLS is set and no port is provided.
		if directTLS {
			options.NoTLS = false
			options.StartTLS = false
			options.Host = net.JoinHostPort(options.Host, "5223")
		} else {
			// Try port 5222 if no port is provided and directTLS is not set.
			options.NoTLS = true
			options.StartTLS = true
			options.Host = net.JoinHostPort(options.Host, "5222")
		}
	}
	// Connect to server
	client, err := options.NewClient()
	if err == nil {
		return client, nil
	}
	return client, fmt.Errorf("failed to connect to server: %w", err)
}
